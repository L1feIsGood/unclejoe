﻿var DataFactory = function($http) {
    return {
        getFree: function() {
            return $http({
                    url: "/data/Free",
                    method: "GET"
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        },
        getMercs: function() {
            return $http({
                    url: "/data/Mercs",
                    method: "GET"
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        },
        getTools: function() {
            return $http({
                    url: "/data/Tools",
                    method: "GET"
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        },
        getConsumabels: function() {
            return $http({
                    url: "/data/Cons",
                    method: "GET"
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        },
        getBundles: function() {
            return $http({
                    url: "/data/Bund",
                    method: "GET"
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        }
    };

};

DataFactory.$inject = ['$http'];


