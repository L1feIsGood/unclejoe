﻿var MarketActionsFactory = function($http) {
    return {
        addToCart: function (productId) {
            console.log("Inside buy!");
            return $http({
                    url: "/cart/Buy",
                    method: "POST",
                    params: productId
                })
                .then(function(data) {
                    return data;
                }, function(err) {
                    return err;
                });
        }
    };

};

MarketActionsFactory.$inject = ['$http'];