﻿var MercPageController = function ($scope, DataFactory) {

    DataFactory.getMercs().then(function (d) {
        $scope.products = d.data;
    }, function () {
        alert('error while loading mercs');
    });

}

FreePageController.$inject = ['$scope', 'DataFactory'];