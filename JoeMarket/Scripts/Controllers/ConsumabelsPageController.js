﻿var ConsumabelsPageController = function ($scope, DataFactory) {

    DataFactory.getConsumabels().then(function (d) {
        $scope.products = d.data;
    }, function () {
        alert('error while loading consumabels');
    });

}

ConsumabelsPageController.$inject = ['$scope', 'DataFactory'];