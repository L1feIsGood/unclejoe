﻿var ToolsPageController = function ($scope, DataFactory) {

    DataFactory.getTools().then(function (d) {
        $scope.products = d.data;
    }, function () {
        alert('error while loading tools');
    });

}

ToolsPageController.$inject = ['$scope', 'DataFactory'];