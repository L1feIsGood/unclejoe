﻿var FreePageController = function ($scope, DataFactory, MarketActionsFactory) {

    DataFactory.getFree().then(function (d) {
        $scope.products = d.data;
    }, function () {
        alert('error while loading free');
    });

    $scope.plusOne = function() {
        console.log("Before factory");
        MarketActionsFactory.addToCart();
        console.log("After factory");
        //$scope.products[index].likes += 1;
    };

    $scope.buy = function() {
        console.log("Before factory");
        MarketActionsFactory.addToCart();
        console.log("After factory");
    };
}

FreePageController.$inject = ['$scope', 'DataFactory', 'MarketActionsFactory'];