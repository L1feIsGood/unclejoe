﻿var BundlesPageController = function ($scope, DataFactory) {

    DataFactory.getBundles().then(function (d) {
        $scope.products = d.data;
    }, function () {
        alert('error while loading bundles');
    });

}

BundlesPageController.$inject = ['$scope', 'DataFactory'];