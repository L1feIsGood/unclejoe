﻿var LandingPageController = function ($scope) {
    
    $scope.models = {
        helloAngular: 'I work!'
    };

    $scope.navbarProperties = {
        isCollapsed: true
    };

    $scope.buy = function() {
        console.log("Before factory");
        MarketActionsFactory.addToCart();
        console.log("After factory");
    };

    $scope.plusOne = function() {
        console.log("Before factory");
        MarketActionsFactory.addToCart();
        console.log("After factory");
        //$scope.products[index].likes += 1;
    };

    console.log("after success!");

}

LandingPageController.$inject = ['$scope'];