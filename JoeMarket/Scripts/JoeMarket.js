﻿var JoeMarket = angular.module('JoeMarket', ['ui.router', 'ui.bootstrap']);

JoeMarket.controller('LandingPageController', LandingPageController);
JoeMarket.controller('FreePageController', FreePageController);
JoeMarket.controller('MercPageController', MercPageController);
JoeMarket.controller('ToolsPageController', ToolsPageController);
JoeMarket.controller('ConsumabelsPageController', ConsumabelsPageController);
JoeMarket.controller('BundlesPageController', BundlesPageController);
JoeMarket.controller('LoginController', LoginController);
JoeMarket.controller('RegisterController', RegisterController);

JoeMarket.factory('DataFactory', DataFactory);
JoeMarket.factory('AuthHttpResponseInterceptor', AuthHttpResponseInterceptor);

var configFunction = function ($stateProvider, $httpProvider, $locationProvider) {

    $locationProvider.hashPrefix('!').html5Mode(true);

    $stateProvider
        .state('stateFree', {
            url: '/Free',
            views: {
                "container": {
                    templateUrl: '/routes/free',
                    controller: 'FreePageController'
                }
            }
        })
        .state('stateMerc', {
            url: '/Mercenaries',
            views: {
                "container": {
                    templateUrl: '/routes/mercs',
                    controller: 'MercPageController'
                }
            }
        })
        .state('stateTools', {
            url: '/Tools',
            views: {
                "container": {
                    templateUrl: '/routes/tools',
                    controller: 'ToolsPageController'
                }
            }
        })
        .state('stateCons', {
            url: '/Consumabels',
            views: {
                "container": {
                    templateUrl: '/routes/cons',
                    controller: 'ConsumabelsPageController'
                }
            }
        })
        .state('stateBund', {
            url: '/Bundles',
            views: {
                "container": {
                    templateUrl: '/routes/bund',
                    controller: 'BundlesPageController'
                }
            }
        })
        .state('stateCart', {
            url: '/Cart',
            views: {
                "container": {
                    templateUrl: '/routes/cart'
                }
            }
        })
        .state('loginRegister', {
            url: '/loginRegister?returnUrl',
            views: {
                "container": {
                    templateUrl: '/Account/Login',
                    controller: LoginController
                }
            }
        });


    $httpProvider.interceptors.push('AuthHttpResponseInterceptor');
}
configFunction.$inject = ['$stateProvider', '$httpProvider', '$locationProvider'];

JoeMarket.config(configFunction);