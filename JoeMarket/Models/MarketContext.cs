﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JoeMarket.Models
{
    public class MarketContext : DbContext
    {

        public MarketContext() : base("UncleJoeMarketDB")
        {
        }

        public DbSet<Category> Categories { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Cart> CartItems { get; set; }
        
    }
}