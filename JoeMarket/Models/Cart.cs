﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace JoeMarket.Models
{
    public class Cart
    {
        [Key]
        public string ItemId { get; set; }

        public string CartId { get; set; }

        public DateTime DateCreated { get; set; }

        public int Quantity { get; set; }
        
        public int ProductId { get; set; }

        public virtual Product Product { get; set; }
    }
}