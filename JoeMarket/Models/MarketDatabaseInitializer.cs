﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace JoeMarket.Models
{
    public class MarketDatabaseInitializer : DropCreateDatabaseIfModelChanges<MarketContext>
    {
        protected override void Seed(MarketContext context)
        {
            GetCategories().ForEach(c => context.Categories.Add(c));
            GetProducts().ForEach(p => context.Products.Add(p));
        }

        private static List<Category> GetCategories()
        {
            var categories = new List<Category> {
                new Category
                {
                    CategoryId = 1,
                    CategoryName = "Free labour force"
                },
                new Category
                {
                    CategoryId = 2,
                    CategoryName = "Mercenaries"
                },
                new Category
                {
                    CategoryId = 3,
                    CategoryName = "Tools"
                },
                new Category
                {
                    CategoryId = 4,
                    CategoryName = "Consumabels"
                },
                new Category
                {
                    CategoryId = 5,
                    CategoryName = "Bundles"
                },
            };

            return categories;
        }

        private static List<Product> GetProducts()
        {
            var products = new List<Product> {
                new Product
                {
                    ProductId = 1,
                    ProductName = "African \"worker\"",
                    Description = "Super powerfull \"worker\" 24/7! Can carry out any \"difficulties!\"",
                    ImagePath="Images/1.1.png",
                    Price = 22.50,
                    CategoryId = 1
               },
                new Product
                {
                    ProductId = 2,
                    ProductName = "Asian \"worker\"",
                    Description = "A bit less powerfull \"worker\", but doesn`t require food! Want to build a freaking Great Wall? This is your choise!",
                    ImagePath="Images/1.2.png",
                    Price = 15.95,
                     CategoryId = 1
               },
                new Product
                {
                    ProductId = 3,
                    ProductName = "Middle east \"worker\"",
                    Description = "If you plan to \"work\" in hot places with massive rocks, dont hesitate to get this one! P.S. Beware the prophets!",
                    ImagePath="Images/1.3.png",
                    Price = 17.99,
                    CategoryId = 1
                },
                new Product
                {
                    ProductId = 6,
                    ProductName = "Professional builder",
                    Description = "Want to be \"Nasyalnikamana\"? Need a swift appartment repairs? Here is your choise!",
                    ImagePath="Images/2.1.png",
                    Price = 30,
                    CategoryId = 2
                },
                new Product
                {
                    ProductId = 7,
                    ProductName = "Simple mercenary",
                    Description = "A little less effective builder. But has his own tools!",
                    ImagePath="Images/2.2.png",
                    Price = 21,
                    CategoryId = 2
                },
                new Product
                {
                    ProductId = 10,
                    ProductName = "Whip",
                    Description = "Yuor \"workers\" doesnt want to work? Cheer them up!",
                    ImagePath="Images/3.1.png",
                    Price = 8.00,
                    CategoryId = 3
                },
                new Product
                {
                    ProductId = 11,
                    ProductName = "The equalizer",
                    Description = "Yuor \"workers\" run too fast? They wont do that again!",
                    ImagePath="Images/3.2.png",
                    Price = 126.00,
                    CategoryId = 3
                },
                new Product
                {
                    ProductId = 12,
                    ProductName = "A pillar",
                    Description = "You need a place to use your whip? Then this is for you!",
                    ImagePath="Images/3.3.png",
                    Price = 6.00,
                    CategoryId = 3
                },
                new Product
                {
                    ProductId = 13,
                    ProductName = "Bullets",
                    Description = "Just load`em up!",
                    ImagePath="Images/4.1.png",
                    Price = 6.25,
                    CategoryId = 4
                },
                new Product
                {
                    ProductId = 14,
                    ProductName = "Fuel",
                    Description = "Dont let your professional workers starve!",
                    ImagePath="Images/4.2.png",
                    Price = 4.95,
                    CategoryId = 4
                },
                new Product
                {
                    ProductId = 16,
                    ProductName = "Young pharaon bundle",
                    Description = "Your time in this world is about to come out? You need a pyramid? Get this bundle of 1000 middle east workers and 100 whips!",
                    ImagePath="Images/5.1.png",
                    Price = 1399.50,
                    CategoryId = 5
                },
                new Product
                {
                    ProductId = 17,
                    ProductName = "Successfull confederate bundle",
                    Description = "The northeners are coming? You still need some cotton? 1 equalizer, 10 whips, 200 bullets and 50 african \"workers\" will help you out!",
                    ImagePath="Images/5.2.png",
                    Price = 1199.50,
                    CategoryId = 5
                },
                new Product
                {
                    ProductId = 18,
                    ProductName = "The great wall bundle",
                    Description = "Want to fence off from barbarians? You need a wall! This bundle of 10000 asian \"workers\" will do the job!",
                    ImagePath="Images/5.3.png",
                    Price = 2999.50,
                    CategoryId = 5
                }
            };

            return products;
        }
    }
}