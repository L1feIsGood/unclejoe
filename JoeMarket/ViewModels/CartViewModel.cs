﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using JoeMarket.Models;

namespace JoeMarket.ViewModels
{
    public class CartViewModel
    {
        public List<Cart> CartItems { get; set; }
        public decimal CartTotal { get; set; }
    }
}