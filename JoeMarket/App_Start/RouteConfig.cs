﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace JoeMarket
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "routeFree",
                url: "routes/Free",
                defaults: new {controller = "Routes", action = "Layout" });

            routes.MapRoute(
                name: "dataFree",
                url: "data/Free",
                defaults: new { controller = "Data", action = "GetFree" });

            routes.MapRoute(
                name: "routeMercenaries",
                url: "routes/Mercs",
                defaults: new {controller = "Routes", action = "Layout" });

            routes.MapRoute(
                name: "dataMercenaries",
                url: "data/Mercs",
                defaults: new { controller = "Data", action = "GetMercs" });

            routes.MapRoute(
                name: "routeTools",
                url: "routes/Tools",
                defaults: new {controller = "Routes", action = "Layout" });

            routes.MapRoute(
                name: "dataTools",
                url: "data/Tools",
                defaults: new { controller = "Data", action = "GetTools" });

            routes.MapRoute(
                name: "routeConsumabels",
                url: "routes/Cons",
                defaults: new { controller = "Routes", action = "Layout" });

            routes.MapRoute(
                name: "dataConsumabels",
                url: "data/Cons",
                defaults: new { controller = "Data", action = "GetConsumabels" });

            routes.MapRoute(
                name: "routeBundles",
                url: "routes/Bund",
                defaults: new { controller = "Routes", action = "Layout" });

            routes.MapRoute(
                name: "dataBundles",
                url: "data/Bund",
                defaults: new { controller = "Data", action = "GetBundles" });

            routes.MapRoute(
                name: "routeCart",
                url: "routes/Cart",
                defaults: new { controller = "Cart", action = "Index" });

            routes.MapRoute(
                name: "login",
                url: "Account/Login",
                defaults: new {controller = "Account", action = "Login"});

            routes.MapRoute(
                name: "register",
                url: "Account/Register",
                defaults: new {controller = "Account", action = "Register"});
            
            routes.MapRoute(
                name: "Default",
                url: "{*url}",
                defaults: new {controller = "Home", action = "Index"});

            routes.MapRoute(
                name: "Buy",
                url: "cart/Buy",
                defaults: new { controller = "Cart", action = "AddToCart" });

        }
    }
}
