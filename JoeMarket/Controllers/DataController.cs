﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using JoeMarket.Models;

namespace JoeMarket.Controllers
{
    public class DataController : Controller
    {
        // GET: Data
        public async Task<ActionResult> GetFree()
        {
            return await GetData("Free labour force");
        }

        public async Task<ActionResult> GetMercs()
        {
            return await GetData("Mercenaries");
        }

        public async Task<ActionResult> GetTools()
        {
            return await GetData("Tools");
        }

        public async Task<ActionResult> GetConsumabels()
        {
            return await GetData("Consumabels");
        }

        public async Task<ActionResult> GetBundles()
        {
            return await GetData("Bundles");
        }

        public async Task<ActionResult> GetData(string categoryName)
        {
            var db = new MarketContext();
            List<Category> qCategories = await db.Categories.ToListAsync();

            var categoryId = qCategories.First(c => c.CategoryName == categoryName)?.CategoryId;

            if (categoryId == null)
            {
                return null;
            }

            List<Product> qProducts = await db.Products.ToListAsync();
            List<Product> products = qProducts.Where(p => p.CategoryId == categoryId).ToList();

            // JSON gets circular reference from Product.Category, so we exlude it from query
            var collection = products.Select(product => new
            {
                product.ProductId,
                product.ProductName,
                product.Description,
                product.ImagePath,
                product.Price,
                product.CategoryId
            });

            return Json(collection, JsonRequestBehavior.AllowGet);
        }
    }
}