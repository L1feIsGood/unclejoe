﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using JoeMarket.BusinessLogic;
using JoeMarket.Models;
using JoeMarket.ViewModels;

namespace JoeMarket.Controllers
{
    public class CartController : Controller
    {
        // GET: Cart
        public ActionResult Index()
        {
            var cart = CartActions.GetCart();

            var viewModel = new CartViewModel
            {
                CartItems = cart.GetCartItems(),
                CartTotal = cart.GetTotal()
            };


            return View(viewModel);
        }

        [HttpPost]
        [AllowAnonymous]
        public bool AddToCart(int id)
        {
            var cart = CartActions.GetCart();

            cart.AddToCart(id);
            
            return true;
        }
    }
}